# exercise 1

# movies = ["The Holy Grail","The Life of Brian", "The Meaning of Life"]
#
# movies.insert(1,1975)
# movies.insert(3, 1979)
# movies.insert(5, 1983)
#
# print(movies)

# exercise 2


movies = ["The Holy Grail", 1975, "Terry Jones & Terr Gilliam", 91,
          ["Graham Chapman", ["Michael Palin", "John Cleese",
                              "Terry Gilliam", "Eric Idle", "Terry Jones"]]]


def print_lol(the_list):
    for each_item in the_list:
        if isinstance(each_item, list):
            print_lol(each_item)
        else:
            print(each_item)


print_lol(movies)
