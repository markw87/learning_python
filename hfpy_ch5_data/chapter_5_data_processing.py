
# first try


def sanitize(time_string):
    if '-' in time_string:
        splitter = '-'
    elif ':' in time_string:
        splitter = ':'
    else:
        return time_string
    (mins, secs) = time_string.split(splitter)
    return (mins+'.'+secs)

try:
    with open("james.txt", 'r') as james_data:
        james_record = james_data.readline().strip().split(sep=',')
        james_record = [sanitize(each_item) for each_item in james_record]
    with open("julie.txt", 'r') as julie_data:
        julie_record = julie_data.readline().strip().split(sep=',')
        julie_record = [sanitize(each_item) for each_item in julie_record]
    with open("mikey.txt", 'r') as mikey_data:
        mikey_record = mikey_data.readline().strip().split(sep=',')
        mikey_record = [sanitize(each_item) for each_item in mikey_record]
    with open("sarah.txt", 'r') as sarah_data:
        sarah_record = sarah_data.readline().strip().split(sep=',')
        sarah_record = [sanitize(each_item) for each_item in sarah_record]


except IOError as err:
    print("Error is found: "+ err)

print(set(james_record))
print(set(julie_record))
print(set(mikey_record))
print(set(sarah_record))

print(sorted(james_record))
print(sorted(julie_record))
print(sorted(mikey_record))
print(sorted(sarah_record))

