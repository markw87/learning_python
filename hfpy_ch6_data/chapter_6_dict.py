
# first try


def sanitize(time_string):
    if '-' in time_string:
        splitter = '-'
    elif ':' in time_string:
        splitter = ':'
    else:
        return time_string
    (mins, secs) = time_string.split(splitter)
    return (mins+'.'+secs)


def get_coach_data(filename):
    try:
        with open(filename) as f:
            data = f.readline()
        return data.strip().split(sep=',')
    except IOError as err:
        print("File Error: " + err)
        return (None)


def get_cooach_data2(filename):
    try:
        with open(filename) as f:
            data = f.readline().strip().split(sep=',')
            return ({'Name':data.pop(0),
                     'DOB':data.pop(0),
                     'scores':str(sorted(set([sanitize(m) for m in data]))[0:3])})
    except IOError as err:
        return (None)

sarah = get_coach_data('sarah2.txt')
sarah_data = {}
sarah_data['Name'] = sarah.pop(0)
sarah_data['DOB'] = sarah.pop(0)
sarah_data['Time'] = sarah

print(sarah_data['Name'])

james = get_cooach_data2('james2.txt')
print(james['Name'])
