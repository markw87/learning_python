## sketch.py


# data = open('sketch.txt')
# for each_line in data:
#     (name, words) = each_line.split(':', 1)
#     print(name,end='')
#     print(" said ", end='')
#     print(words, end='')
#
#
# data.close()


## Try exceptions

try:
    data = open('sketch.txt')
    for each_line in data:
        try:
            (name, words) = each_line.split(':', 1)
            print(name, end='')
            print(" said ", end='')
            print(words, end='')
        except ValueError:
            pass
    data.close()
except FileNotFoundError:
    print('the data file is missing!')

