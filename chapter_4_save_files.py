# exercise


import os
import pickle

man = []
other = []

# first try
try:
    data = open('sketch.txt')
    for each_line in data:
        try:
            (name, words) = each_line.split(':', 1)
            words = words.strip()
            if name == 'Man':
                man.append(words)
            elif name == 'Other Man':
                other.append(words)

        except ValueError:
            pass
    data.close()
except FileNotFoundError:
    print('the data file is missing!')

print(man)
print(other)

# second try
try:
	with open('its.txt', 'w') as data:
		print('it is a test with with', file = data)
except IOError as err:
	print('File Error: ' + str(err))

# third try

try:
	with open('man_data.txt','w') as man_file:
		print(man, file = man_file)
	with open('other_data.txt', 'w') as other_file:
		print(other, file = other_file)

except IOError as err:
	print('File Error: ' + str(err))


# last try

try:
	with open('man_data.txt', 'wb') as man_file:
		pickle.dump(man, man_file)
	with open('other_data.txt','wb') as other_file:
		pickle.dump(other, other_file)

except IOError as err:
	print("File Error: " + str(err))
except Pickle.PickleError as perr:
	print('Pickle Error: ' + str(perr))

	


