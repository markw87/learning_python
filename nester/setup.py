from distutils.core import setup

setup(

    name     ='nester',
    version      ='1.0.0',
    py_modules=['nester'],
    author='Mark Wang',
    author_email='markw1987@163.com',
    url='http://www.baidu.com',
    description='A simple printer for nested list',

)
